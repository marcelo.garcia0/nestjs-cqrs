export class CreateUserCommand {
  constructor(
    public readonly name: string,
    public readonly email: string,
    public readonly password: string,
    public readonly favorite_color: string,
  ) {
    this.name = name;
    this.email = email;
    this.password = password;
    this.favorite_color = favorite_color;
  }
}
