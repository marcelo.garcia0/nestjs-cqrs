import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { ExampleUser } from 'src/user-example/entities/example-user.entity';
import { Repository } from 'typeorm';
import { CreateUserCommand } from '../create-user.command';

@CommandHandler(CreateUserCommand)
export class CreateUserHandler implements ICommandHandler<CreateUserCommand> {
  constructor(
    @InjectRepository(ExampleUser)
    private userRepository: Repository<ExampleUser>,
  ) {}
  async execute(command: CreateUserCommand) {
    return await this.userRepository.insert(new ExampleUser(command));
  }
}
