import { CommandHandler, ICommandHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { ExampleUser } from 'src/user-example/entities/example-user.entity';
import { Repository } from 'typeorm';
import { UpdateUserCommand } from '../update-user.command';

@CommandHandler(UpdateUserCommand)
export class UpdateUserHandler implements ICommandHandler<UpdateUserCommand> {
  constructor(
    @InjectRepository(ExampleUser)
    private userRepository: Repository<ExampleUser>,
  ) {}
  async execute(command: UpdateUserCommand) {
    return await this.userRepository.save(new ExampleUser(command));
  }
}
