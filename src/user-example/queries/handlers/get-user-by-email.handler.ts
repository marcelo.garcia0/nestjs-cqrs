import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { ExampleUser } from 'src/user-example/entities/example-user.entity';
import { Repository } from 'typeorm';
import { GetUserByEmailQuery } from '../get-user-by-email.query';

@QueryHandler(GetUserByEmailQuery)
export class GetUserByEmailHandler
  implements IQueryHandler<GetUserByEmailQuery>
{
  constructor(
    @InjectRepository(ExampleUser) private repository: Repository<ExampleUser>,
  ) {}

  async execute(query: GetUserByEmailQuery): Promise<any> {
    return await this.repository.findOne({ email: query.email });
  }
}
