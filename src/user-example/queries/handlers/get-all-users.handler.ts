import { IQueryHandler, QueryHandler } from '@nestjs/cqrs';
import { InjectRepository } from '@nestjs/typeorm';
import { ExampleUser } from 'src/user-example/entities/example-user.entity';
import { Repository } from 'typeorm';
import { GetAllUsersQuery } from '../get-all-users.query,';

@QueryHandler(GetAllUsersQuery)
export class GetAllUsersHandler implements IQueryHandler<GetAllUsersQuery> {
  constructor(
    @InjectRepository(ExampleUser) private repository: Repository<ExampleUser>,
  ) {}

  async execute(): Promise<any> {
    return await this.repository.find();
  }
}
