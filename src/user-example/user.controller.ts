import { Body, Controller, Get, Param, Post } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateUserDto } from 'src/dto/ExampleDto';
import { CreateUserCommand } from './commands/create-user.command';
import { GetAllUsersQuery } from './queries/get-all-users.query,';
import { GetUserByEmailQuery } from './queries/get-user-by-email.query';
// eslint-disable-next-line @typescript-eslint/no-var-requires
const bcrypt = require('bcrypt');

@Controller('user-example')
export class UserExampleController {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly queryBus: QueryBus,
  ) {}

  @Get()
  async getAllUsers() {
    try {
      return await this.queryBus.execute(new GetAllUsersQuery());
    } catch (err) {
      throw new Error(err.message);
    }
  }

  @Get(':email')
  async getUserByEmail(@Param() payload: { email: string }) {
    return await this.queryBus.execute(new GetUserByEmailQuery(payload.email));
  }

  @Post()
  async createUser(@Body() userDto: CreateUserDto) {
    try {
      const salt = bcrypt.genSaltSync(10);
      await this.commandBus.execute(
        new CreateUserCommand(
          userDto.email,
          userDto.name,
          bcrypt.hashSync(userDto.password, salt),
          userDto.favorite_color,
        ),
      );
      return 'User Added';
    } catch (err) {
      throw new Error(err.message);
    }
  }
}
