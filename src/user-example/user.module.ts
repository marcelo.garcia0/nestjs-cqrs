import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreateUserHandler } from './commands/handlers/create-user.handler';
import { UpdateUserHandler } from './commands/handlers/update-user.handler';
import { ExampleUser } from './entities/example-user.entity';
import { GetAllUsersHandler } from './queries/handlers/get-all-users.handler';
import { GetUserByEmailHandler } from './queries/handlers/get-user-by-email.handler';
import { UserExampleController } from './user.controller';

@Module({
  imports: [TypeOrmModule.forFeature([ExampleUser]), CqrsModule],
  controllers: [UserExampleController],
  providers: [
    CreateUserHandler,
    UpdateUserHandler,
    GetAllUsersHandler,
    GetUserByEmailHandler,
  ],
})
export class UserExampleModule {}
