import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserExampleModule } from './user-example/user.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    UserExampleModule,
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: process.env.DATABASE_HOST,
      port: parseInt(process.env.DATABASE_PORT),
      username: process.env.DATABASE_USERNAME,
      password: process.env.DATABASE_PASSWORD,
      database: process.env.DATABASE_NAME,
      entities: ['dist/**/*.entity{.ts,.js}'],
      migrations: [__dirname + './migrations/*{.ts,.js}'],
      synchronize: false,
    }),
  ],
})
export class AppModule {}
