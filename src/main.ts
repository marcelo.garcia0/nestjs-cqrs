import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { enableSwagger } from './utils/enableSwagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  enableSwagger(app);
  await app.listen(8000);
}
bootstrap();
