export class CreateUserDto {
  name: string;
  email: string;
  password: string;
  favorite_color: string;
}
